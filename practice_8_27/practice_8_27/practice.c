#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//print(char* s)
//{
//	if (*s)
//	{
//		print(++s);
//		printf("%c", *s);
//	}
//}
//
//int main()
//{
//	char str[] = "Geneius";
//	print(str);
//	return 0;
//}

//int fun(int x)
//{
//	return (x == 1) ? 1 : (x + fun(x - 1));
//}
//int main()
//{
//	printf("%d", fun(10));
//	return 0;
//}

//int pivotIndex(int* nums, int numsSize) 
//{
//	int sz = numsSize / 2;
//	int sum1 = 0, sum2 = 0,i=0,j=0;
//	while (1)
//	{
//		for ( i = 0; i < sz; i++)
//		{
//			sum1 += *(nums + i);
//		}
//		for ( j = sz + 1; j < numsSize; j++)
//		{
//			sum2 += *(nums + j);
//		}
//		if (sum1 == sum2)
//			return sz;
//		else 
//			return -1;
//
//		sum1 = 0;
//		for (i = 1; i < numsSize; i++)
//			sum1 += *(nums + i);
//		if (*nums == sum1)
//			return 0;
//
//		sum1 = 0;
//		for (i = 0; i < numsSize - 1; i++)
//			sum1 += *(nums + i);
//		if (sum1 == *(nums + numsSize))
//			return 0;
//		
//	}
//}
//
//int main()
//{
//	int n = 0;
//	int arr[2000] = { 0 };
//	scanf("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	printf("%d", pivotIndex(arr, n));
//}

//int majorityElement(int* nums, int numsSize) {
//	int max1 = 0, max2 = 0, num = 0, count = 0;
//	for (int i = 0; i < numsSize; i++)
//	{
//		if (num == *(nums + i))
//			continue;
//		count = 1;
//		for (int j = 0; j < numsSize; j++)
//		{
//			if ((*(nums + i) == *(nums + j)) && (j != i))
//			{
//				count++;
//			}
//		}
//		if (max1 < count)
//		{
//			num = *(nums + i);
//			max1 = count;
//			max2 = *(nums + i);
//		}
//	}
//	return max2;
//}
//int main()
//{
//	int arr[5000] = { 0 };
//	int n = 0;
//	scanf("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	printf("%d", majorityElement(arr, n));
//}

//int main()
//{
//	int a[12] = { 1,2,3,4,5,6,7,8,9,10,11,12 }, * p[4], i;
//	for (i = 0; i < 4; i++)
//		p[i] = &a[i * 3];
//	printf("%d\n",p[3][2]);
//	return 0;
//}

//int main()
//{
//	char a = 'a', b;
//	printf("%c,", ++a);
//	printf("%c\n", b = a++);
//	return 0;
//}

int* selfDividingNumbers(int left, int right, int* returnSize) 
{
    int z = 0;
    for (int i = left; i <= right; i++)
    {
        int n = i;

        while (n)
        {
            if (((n % 10) == 0) || (i % (n % 10)) != 0)
                break;
            n /= 10;
        }
        if (n == 0)
        {
            *(returnSize + z) = i;
            z++;
        }
    }
    return returnSize;
}

int main()
{
    int arr[1000] = { 0 };
    int left = 0, right = 0;
    scanf("%d %d", &left, &right);
    int* p = selfDividingNumbers(left, right, arr);
    return 0;
}