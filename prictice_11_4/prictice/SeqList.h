#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#define  _CRT_SECURE_NO_WARNINGS 1



typedef int SLDateType;
typedef struct SeqList
{
	SLDateType* a;
	int size;	  //有效空间
	int capacity; //空间容量
}SeqList;

// 对数据的管理:增删查改 
// 
//初始化
void SeqListInit(SeqList* ps);

//销毁
void SeqListDestroy(SeqList* ps);

//打印
void SeqListPrint(SeqList* ps);

//空间
void SLCheckCapacity(SeqList* ps);

// 尾插
void SeqListPushBack(SeqList* ps, SLDateType x);
// 头插
void SeqListPushFront(SeqList* ps, SLDateType x);
// 头删
void SeqListPopFront(SeqList* ps);
// 尾删
void SeqListPopBack(SeqList* ps);


// 顺序表查找
int SeqListFind(SeqList* ps, SLDateType x);
// 顺序表在pos位置插入x
void SeqListInsert(SeqList* ps, int pos, SLDateType x);
// 顺序表删除pos位置的值
void SeqListErase(SeqList* ps, int pos);