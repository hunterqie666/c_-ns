#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int SLNDataType;
typedef struct SListNode
{
	SLNDataType val;
	struct SListNode* next;
}SLNode;





// 单链表打印
void SLTPrint(SLNode* phead);

// 单链表尾插
void SLTPushBack(SLNode** pphead, SLNDataType x);

// 单链表的头插
void SLTPushFront(SLNode** pphead, SLNDataType x);

//单链表的尾删
void SLTPopBack(SLNode** pphead);

// 单链表头删
void SLTPopFront(SLNode** pphead);


// 单链表查找
SLNode* SLTFind(SLNode* phead, SLNDataType x);

// 在pos的前面插入
SLNode* SLTInsert(SLNode** pphead, SLNode* pos, SLNDataType x);

// 删除pos位置
void SLTErase(SLNode** pphead, SLNode* pos);
void SLTDestroy(SLNode** pphead);