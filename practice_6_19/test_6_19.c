#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//#define n 5
//int main()
//{
//	int arr[n] = { 0 };
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int a = 0,  j = 0,tmp=0;
//	for (a = 0; a < n; a++)
//	{
//		for (j = a+1; j < n; j++)
//		{
//			if (arr[a] > arr[j])
//			{
//				tmp = arr[a];
//				arr[a] = arr[j];
//				arr[j] = tmp;
//			}
//		}
//	}
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int arr[8] = { 0 };
//	int i = 0;
//	for (i = 0; i < 8; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int a = 0;
//	scanf("%d", &a);
//	for (i = 0; i < 8; i++)
//	{
//		if (a == arr[i])
//		{
//			printf("%d", i);
//			break;
//		}
//	}
//	if (i == 8)
//	{
//		printf("%d", -1);
//	}
//	return 0;
//}



//int main()
//{
//	//数组名是首元素的地址
//	// 1. sizeof(数组名) - 数组名表示整个数组
//	// 2．& 数组名–数组名表示整个数组
//	// 一维数组
//	int a[] = { 1,2,3,4 };//4*4 = 16
//	printf("%d\n", sizeof(a));//sizeof(数组名)-计算的是数组总大小-单位是字节-16
//	printf("%d\n", sizeof(a + 0));//4/8-数组名这里表示收元素的值，a+8还是首元素地址，地址的大小就是4/8个字节
//    printf("%d\n", sizeof(*a));//4～数组名表示首元素地址，*a就是首元素，sizeof(*a)就是4
//	printf("%d\n", sizeof(a + 1));//4/8数组名这里表示收元素的值，a+1第2个元素的地址，地址的大小就是4/8个字节
//    printf("%d\n", sizeof(a[1])); //4 -第2个元素的大小
//	printf("%d\n", sizeof(&a));//4/8 &a取出的是数组的地址，但是数组的地址那也是地址，地址的大小就是4/8个字节
//	printf("%d\n", sizeof(*&a));//16- &a数组的地址，数组的地址解引用访问的数组，sizeof计算的就是数组的大小单位是字节
//    printf("%d\n", sizeof(&a + 1));//4/8&a是数组的地址，&a+1虽然地址跳过整个数组，但还是地址，所以是4/8个字节
//	printf("%d\n", sizeof(&a[0]));//4/8 &a[0]是第一个元素的地址
//	printf("%d\n", sizeof(&a[0] + 1));//4/8&a[0]+1 是第二个元素的地址
//	return 0;
//}

//int main()
//{
//	char arr[] = {'a', 'b', 'c', 'd', 'e', 'f'};
//	printf("%d\n", sizeof(arr) );//sizeof计算机的是数组大小，6*1 = 6字节
//	printf("%d\n", sizeof(arr + 0));//4/8 arr是首元素的地址，arr+8还是首元素的地址地址的大小是4/8字节
//	printf("%d\n", sizeof(*arr));//arr是首元素的地址，*arr就是首元素，首元素是字符大小是一个字节
//	printf(" %d\n", sizeof(arr[1])); //1
//	printf("%d\n", sizeof(&arr)); //&arr虽然是数组的地址，但还是地址，地址大小是4 / 8个字节
//	printf("%d\n", sizeof(&arr + 1));//&arr+1是跳讨整个数组后的地址．地址大小是4/8个字节
//	return 0;
//}