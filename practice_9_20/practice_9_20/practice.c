#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>


//
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//    void* ret = dest;
//    assert(dest != NULL);
//    assert(src != NULL);
//
//    while (num--)
//    {
//        *(char*)dest = *(char*)src;
//        ++(char*)dest;
//        ++(char*)src;
//    }
//    return ret;
//}
//int main()
//{
//    int arr1[] = { 1,2,3,4,5 };
//    int arr2[5] = { 0 };
//    my_memcpy(arr2, arr1, sizeof(arr1));
//    return 0;
//}


//void* my_memmove(void* dest, const void* src, size_t count)
//{
//    void* ret = dest;
//    assert(dest != NULL);
//    assert(src != NULL);
//    if (dest < src)
//    {
//        //前->后
//        while (count--)
//        {
//            *(char*)dest = *(char*)src;
//            ++(char*)dest;
//            ++(char*)src;
//        }
//    }
//    else
//    {
//        //后—>前
//        while (count--)
//        {
//            *((char*)dest + count) = *((char*)src + count);
//        }
//    }
//    return ret;
//}
//
//
//int main()
//{
//    int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//    int i = 0;
//    //my_memcpy(arr+2,arr,20);
//    my_memmove(arr + 2, arr, 20);
//    for (i = 0; i < 10; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}


//int main()
//{
//    char arr[10] = "";
//    memset(arr, '#', 10);
//    int arr[10] = { 0 };
//    //40个字节
//    //01 01 01 01 01 01 01 01 01 01 00 00 00 00 00 ....
//    memset(arr, 1, 10);
//    return 0;
//}


int main()
{
    int arr1[] = { 1,2,3,4,5 };
    int arr2[] = { 1,2,5,4,3 };
    int ret = memcmp(arr1, arr2, 8);
    printf("%d\n", ret);
    return 0;
}