#include "Queue.h"

void QueueInit(Queue* q)
{
	assert(q);

	q->phead = NULL;
	q->ptail = NULL;
	q->size = 0;
}

void QueueDestroy(Queue* q)
{
	assert(q);

	QNode* cur = q->phead;
	while (cur)
	{
		QNode* next = cur->next;
		free(cur);
		cur = next;
	}
	q->phead = NULL;
	q->ptail = NULL;
	q->size = 0;
}

void QueuePush(Queue* q, QDataType data)
{
	assert(q);

	QNode* newnode = (QNode*)malloc(sizeof(QNode));

	if (newnode == NULL)
	{
		perror("malloc fail!!!");
	}

	newnode->val = data;
	newnode->next = NULL;

	if (q->ptail == NULL)
	{
		q->ptail = newnode;
		q->phead = newnode;
	}
	else
	{
		q->ptail->next = newnode;
		q->ptail = newnode;
	}
	q->size++;
}

void QueuePop(Queue* q)
{
	assert(q);
	assert(q->phead);

	QNode* del = q->phead;
	q->phead = q->phead->next;
	free(del);
	del = NULL;

	if (q->phead == NULL)
	{
		q->ptail = NULL;
	}

	q->size--;
}

QDataType QueueFront(Queue* q)
{
	assert(q);
	assert(q->phead);

	return q->phead->val;
}

QDataType QueueBack(Queue* q)
{
	assert(q);
	assert(q->phead);

	return q->ptail->val;
}

int QueueSize(Queue* q)
{
	assert(q);

	return q->size;
}

int QueueEmpty(Queue* q)
{
	assert(q);

	return q->phead == NULL;
}