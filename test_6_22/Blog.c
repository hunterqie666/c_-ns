#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

////#define max 100 
////int main()
////{
//// int a = max;//定义标识符常量
////return 0;
////}
//int MAX(int x, int y)
//{
//	if (x > y)
//		return x;
//	else
//		return y;
//}//函数
//#define MAX(X,Y) (X>Y?X:Y)//可以定义宏 -带参数
//int main()//#define 
//{
//
//	int a = 10;
//	int b = 20;
//	//函数
//	int max = 0;
//	max = MAX(a, b);
//	printf("%d\n", max);
//	//宏的方式
//	max = MAX(a, b);
//	//max=（a>b? a:b）
//	printf("%d\n", max);
//	return 0;
//}

//#define M 10
//int main()
//{
//	int arr[M] = { 0 };
//	return 0;
//}

//int main()
//{
//	//& -->取地址操作符
//	//* -->解引用操作符
//	int a = 10;//在内存中开辟一块名叫a空间 往这个空间里面存放10
//	int* p = &a;//这里我们对变量a，取出它的地址，把a的地址存放在的*p里面，这样我们可以通过访问p去了解a。
//	//a变量占用4个字节的空间，这里是将a的4个字节的第一个字节的地址存放在p变量中，p就是一个之指针变量。 
//	*p = 20;//我们通过解引用p去访问a，并且把a的内容改为了20.
//	printf("%d", a);//20
//	return 0;
//}

//int main()
//{
//	int a = 0x11223344;
//	/*int* pa = &a;
//	char* pc = &a;
//	printf("%p\n", pa);
//	printf("%p\n", pc);*///pa,pc输出结果一模一样
//	char* pc = &a;
//	*pc = 0;
//	return 0;
//}//指针类型决定了指针进行解引用操作的时候，能够访问空间的大小
////int* pc==>4  char* pc==>1 ==>看数据类型大小


//int main()
//{
//    printf("%d\n", sizeof(char*));//8
//    printf("%d\n", sizeof(int*));//8
//    printf("%d\n", sizeof(double*));//8
//    return 0;
//}

//struct Peo //struct：结构体关键字 Peo：标签(标签可以随便起)
//{
//    char name[20];//名字
//    int age;      //年龄
//    char sex[5];  //性别
//    //成员变量 变量列表
//};//这里的;不要忘记
//
//int main()
//{
//    struct Peo hunter = { "ctx",10,"男" };//利用结构体关键字创建了一个hunter变量，并且初始化。
//    printf("name=%s age=%d sex=%s\n", hunter.name, hunter.age, hunter.sex);
//   
//
//    struct Peo *s = &hunter;
//    printf("name=%s age=%d sex=%s", s->name, s->age, s->sex);
//    //结构体变量 .  结构体成员
//    //结构体指针 -> 结构体成员
//    return 0;
//}