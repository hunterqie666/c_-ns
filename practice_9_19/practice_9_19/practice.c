#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

//int main()
//{
//	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0 ,j = 0;
//	for ( i = 0; i < 3; i++)
//	{
//		if (arr[i][2] < n)
//		{
//			continue;
//		}
//		else if(arr[i][2] >= 2)
//		{
//			for ( j = 0; j < 3; j++)
//			{
//				if (arr[i][j] == n)
//				{
//					printf("变量n存在\n");
//					goto exit;
//				}
//			}
//			if (j == 3)
//			{
//				printf("变量n不存在\n");
//				break;
//			}
//		}
//	}
//	if (i == 3)
//	{
//		printf("变量n不存在\n");
//	}
//exit:
//	return 0;
//}



//void print(int* arr,int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//	printf("\n");
//}
//
//void cmp_int(const void* e1, const void* e2)
//{
//	return *(int*)e1 - *(int*)e2;
//}
//void test1()
//{
//	int arr[] = { 3,4,1,2,5 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr, sz);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	print(arr, sz);
//}
//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//int cmp_stu_by_age(const void* e1, const void* e2)
//{
//	return ((struct Stu*)e1)->age - ((struct Stu*)e2)->age;
//}
//
//void test2()
//{
//	struct Stu arr[] = { {"zhanhsan", 20}, {"lisi", 30}, {"wangwu", 12} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_stu_by_age);
//}
//
//
//int cmp_stu_by_name(const void* e1, const void* e2)
//{
//	return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
//}
//
//void test3()
//{
//	struct Stu arr[] = { {"zhanhsan", 20}, {"lisi", 30}, {"wangwu", 12} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_stu_by_name);
//}
//int main()
//{
//	test1();
//	test2();
//	test3();
//	return 0;
//}


//char* my_strcpy(char* dest, const char* src)
//{
//	char* ret = dest;
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = { 0 };
//	char arr2[] = "ctxyyds";
//	printf("%s\n", my_strcpy(arr1, arr2));
//	return 0;
//}


//int my_strlen(const char* str)
//{
//    int count = 0;
//    while (*str != '\0')
//    {
//        count++;
//        str++;
//    }
//    return count;
//}
//
//int main()
//{
//    char arr[] = "ctxyyds";
//    int len = my_strlen(arr);
//    printf("%d\n", len);
//    return 0;
//}


//int my_strcmp(char* str1, char* str2)
//{
//    while (*str1 == *str2)
//    {
//        if (*str1 == '\0')
//        {
//            return 0;
//        }
//        *str1++;
//        *str2++;
//    }
//    if (*str1 < *str2)
//    {
//        return -1;
//    }
//    if (*str1 > *str2)
//    {
//        return 1;
//    }
//}
//
//int main()
//{
//    char* p1 = "ctxyyds";
//    char* p2 = "ctxnb";
//    int ret = my_strcmp(p1, p2);
//    printf("%d\n", ret);
//    return 0;
//}


//
//char* my_strstr(const char* p1, const char* p2)
//{
//    assert(p1 != NULL);
//    assert(p2 != NULL);
//    char* s1 = NULL;
//    char* s2 = NULL;
//    char* cur = (char*)p1;
//    if (*p2 == '\0')
//    {
//        return (char*)p1;
//    }
//    while (*cur)
//    {
//        s1 = cur;
//        s2 = (char*)p2;
//        while ((*s1 != '\0') && (*s2 != '\0') && (*s1 == *s2))
//        {
//            s1++;
//            s2++;
//        }
//        if (*s2 == '\0')
//        {
//            return cur;//找到字符串
//        }
//        if (*s1 == '\0')
//        {
//            return NULL;
//        }
//        cur++;
//    }
//    return NULL;//找不到字符串
//}
//
//int main()
//{
//    char* p1 = "ctxyyds";
//    char* p2 = "yyds";
//
//    char* ret = my_strstr(p1, p2);
//
//    if (ret == NULL)
//    {
//        printf("字符串不存在\n");
//    }
//    else
//    {
//        printf("%s\n", ret);
//    }
//    return 0;
//}


//char* my_strcat(char* dest, const char* src)
//{
//	assert(dest != NULL && src != NULL); 
//	char* ret = dest;   
//	while (*dest != '\0')  
//	{
//		dest++;
//	}
//	while ((*dest++ = *src++) != '\0')
//	{
//		;
//	}
//	return ret;  
//}
//int main()
//{
//	char str[30] = "ctx";
//	printf("%s\n", my_strcat(str, "yyds"));
//	return 0;
//}


//int main()
//{
//	int aa[2][5] = { 10,9,8,7,6,5,4,3,2,1 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}


//int main()
//{
//	int a[5] = { 5, 4, 3, 2, 1 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d", *(a + 1), *(ptr - 1));
//	return 0;
//}


int cmp(void* e1, void* e2)  
{
	return *((int*)e1) - *((int*)e2);
}
void swap(char* p1, char* p2, int width)   
{
	int t = 0;
	int i = 0;
	for (i = 0; i < width; i++)
	{
		t = *p1;
		*p1 = *p2;
		*p2 = t;
		p1++;
		p2++;
	}
}
void bubble_sort(void* arr, int sz, int width, int(*cmp)(void* e1, void* e2))
{
	int i = 0;
	int j = 0;
	for (i = 0; i < sz - 1; i++)
	{
		for (j = 0; j < sz - 1 - i; j++)   
		{
			if (cmp((char*)arr + (j * width), (char*)arr + (j + 1) * width) > 0)
			{
				swap((char*)arr + (j * width), (char*)arr + (j + 1) * width, width);
			}
		}
	}
}
int main()
{
	int arr[] = { 5,2,4,1,3 };
	int sz = sizeof(arr) / sizeof(arr[0]);  
	int i = 0;
	bubble_sort(arr, sz, sizeof(arr[0]), cmp);
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}
