#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int missingNumber(int* nums, int numsSize)
//{
//    int x = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        x ^= nums[i];
//    }
//
//    for (int i = 0; i <= numsSize; i++)
//    {
//        x ^= i;
//    }
//
//    return x;
//}
//
//
//
//
//int missingNumber(int* nums, int numsSize)
//{
//    int n = numsSize;
//
//    int sum = ((0 + n) * (n + 1)) / 2;
//
//    for (int i = 0; i < numsSize; i++)
//    {
//            sum -= nums[i];
//    }
//    return sum;
//}
//
//int main()
//{
//    int num = 0;
//    scanf("%d", &num);
//    int arr[100] = { 0 };
//    for (int i = 0; i < num; i++)
//    {
//        scanf("%d",&arr[i]);
//    }
//    int z = missingNumber(arr, num);
//    printf("%d", z);
//	return 0;
//}

//void rotate(int* nums, int numsSize, int k)
//{
//    for (int i = 0; i < k; i++)
//    {
//        int z = nums[numsSize - 1];
//        for (int j = numsSize - 1; j > 0; j--)
//        {
//            nums[j] = nums[j - 1];
//        }
//        nums[0] = z;
//    }
//}

void rotate(int* nums, int numsSize, int k)
{
    int arr[numsSize];

    for (int i = 0; i < numsSize; i++)
    {
        arr[(i + k) % numsSize] = nums[i];
    }

    for (int i = 0; i < numsSize; i++)
    {
        nums[i] = arr[i];
    }

}

int main()
{
	int arr[] = { 1,2,3,4,5,6,7 };
	int k = 3;
    rotate(arr, 7, k);
    for (int i = 0; i < 7; i++)
    {
        printf("%d ",arr[i]);
    }
	return 0;
}