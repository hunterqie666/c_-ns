#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int main()
//{
//	int c_len = 0, c_wide = 0, y_len = 0, z_len;
//
//	scanf("%d %d", &c_len, &c_wide);
//	scanf("%d", &y_len);
//	scanf("%d", &z_len);
//
//	int c_area = c_len * c_wide;
//
//	int z_area = z_len * z_len;
//
//	double y_area = 3.14 * (y_len * y_len);
//	y_area *= 100;
//	int z = y_area % 100;
//
//	printf("%d\n%.2lf\n%d", c_area, y_area, z_area);
//	
//	return 0;
//}

//int main() 
//{
//    char c;
//    scanf("%c", &c);
//    int i = 0, j = 0, k = 0;
//    for (i = 0; i < 5; i++)
//    {
//        for (j = 4 - i; j > 0; j--)
//        {
//            printf(" ");
//        }
//        for ( k = 0; k <= i; k++)
//        {
//            printf("%c ", c);
//        }
//        printf("\n");
//    }
//    return 0;
//}


//int main()
//{
//    float a = 0.0;
//    float b = 0.0;
//    float c = 0.0;
//    while (scanf("%f %f %f", &a, &b, &c) == 3)
//    {
//        if (a != 0)
//        {
//            float disc = b * b - 4 * a * c;
//            if (disc > 0.0)
//            {
//                printf("x1=%.2f;x2=%.2f\n", (-b - sqrt(disc)) / (2 * a), (-b + sqrt(disc)) / (2 * a));
//            }
//            else if (disc < 0.0)
//            {
//                printf("x1=%.2f-%.2fi;x2=%.2f+%.2fi\n", (-b) / (2 * a), sqrt(-disc) / (2 * a), (-b) / (2 * a), sqrt(-disc) / (2 * a));
//            }
//            if ((-b) / (2 * a) == -0)
//            {
//                printf("x1=x2=%.2f\n", 0.00);
//            }
//            else
//            {
//                printf("x1=x2=%.2f\n", (-b) / (2 * a));
//            }
//        }
//        else
//        {
//            printf("Not quadratic equation\n");
//        }
//    }
//    return 0;
//}

int main()
{
    int c;
    while ((scanf("%d", &c)) == 1)
    {
        int i = 0, j = 0, k = 0;
        for (i = 0; i < c; i++)
        {
            for (j = c - i; j > 1; j--)
            {
                printf(" ");
            }
            for (k = 0; k <= i; k++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
    return 0;
}