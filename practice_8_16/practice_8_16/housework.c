#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include<stdlib.h>

//int main()
//{
//    int i = 0, j = 0;
//    int a[10][10];
//    for (i = 0; i < 10; i++) 
//    {
//        a[i][0] = 1;
//        a[i][i] = 1;
//    }
//    for (i = 2; i < 10; i++)
//        for (j = 1; j < i; j++)
//        {
//            a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
//        }
//    for (i = 0; i < 10; i++) 
//    {
//        for (j = 0; j <= i; j++)
//        {
//            printf("%5d", a[i][j]);
//        }
//        printf("\n");
//    }
//}

//void swap(int* s1, int* s2)
//{
//    int t;
//    t = *s1; *s1 = *s2; *s2 = t;
//}
//
//int main(void)
//{
//    int a, b, c;
//    int* p1, * p2, * p3;
//    scanf("%d %d %d", &a, &b, &c);
//    p1 = &a;
//    p2 = &b;
//    p3 = &c;
//    if (a > b)
//        swap(p1, p2);
//    if (a > c)
//        swap(p1, p3);
//    if (b > c)
//        swap(p2, p3);
//    printf("%d %d %d\n", a, b, c);
//}

//void fun(int* s, int n)
//{
//    int i = 0;
//    int max = s[0];
//    int a = 0;
//    for (i = 0; i < n; i++)
//    {
//        if (s[i] > max)
//        {
//            max = s[i];
//            a = i;
//        }
//    }
//    s[a] = s[0];
//    s[0] = max;
//    int j = 0;
//    int min = s[n - 1];
//    int b = n - 1;
//    for (j = 0; j < n; j++)
//    {
//        if (s[j] < min)
//        {
//            min = s[j];
//            b = j;
//        }
//    }
//    s[b] = s[n - 1];
//    s[n - 1] = min;
//}
//
//void print(int* s, int n)
//{
//    int i;
//    for (i = 0; i < n; i++)
//    {
//        printf("%d ", s[i]);
//    }
//    printf("\n");
//}
//
//int main()
//{
//    int arr[20] = { 0 };
//    int i = 0, n = 0;
//
//    scanf("%d", &n);
//
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    fun(arr, n);
//    print(arr, n);
//    return 0;
//}