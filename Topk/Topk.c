//#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//#include<stdlib.h>
//#include<assert.h>
//#include<time.h>
//
//
//void Swap(int* p1, int* p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//
//
//void AdjustDown(int* a, int n, int parent)
//{
//	int child = parent * 2 + 1;
//	while (child < n)
//	{
//		if (child + 1 < n && a[child + 1] < a[child])
//		{
//			child++;
//		}
//		if (a[parent] > a[child])
//		{
//			Swap(&a[parent], &a[child]);
//		}
//		parent = child;
//		child = parent * 2 + 1;
//	}
//}
//
//
//void AdjustUp(int* a, int child)
//{
//	int parent = (child - 1) / 2;
//	while (child > 0)
//	{
//		if (a[parent] > a[child])
//		{
//			Swap(&a[parent], &a[child]);
//		}
//		child = parent;
//		parent = (child - 1) / 2;
//	}
//}
//
//void CreateNDate()
//{
//	// 造数据
//	int n = 10000;
//	srand(time(0));
//	const char* file = "data.txt";
//	FILE* fin = fopen(file, "w");
//	if (fin == NULL)
//	{
//		perror("fopen error");
//		return;
//	}
//
//	for (size_t i = 0; i < n; ++i)
//	{
//		int x = rand() % 1000000;
//		fprintf(fin, "%d\n", x);
//	}
//	fclose(fin);
//}
//
//
//void PrintTopK(const char* file, int k)
//{
//	int* topK = (int*)malloc(sizeof(int) * k);
//	assert(topK);
//	FILE* fout = fopen(file, "r");
//
//	if (fout == NULL) 
//	{
//		perror("open fail");
//		return;
//	}
//
//	for (int i = 0; i < k; i++) 
//	{
//		fscanf(fout, "%d", &topK[i]);
//	}
//
//	for (int i = (k - 2) / 2; i < k; i++)
//	{
//		AdjustDown(topK, k, i);
//	}
//
//	/*for (int i = k - 1; i > 0; i--)
//	{
//		AdjustUp(topK, i);
//	}*/
//
//	int val = 0;
//	int ret = fscanf(fout, "%d", &val);
//	while (ret != EOF)
//	{
//		if (val > topK[0])
//		{
//			topK[0] = val;
//			AdjustDown(topK, k, 0);
//		}
//		ret = fscanf(fout, "%d", &val);
//	}
//
//	for (int i = 0; i < k; i++)
//	{
//		printf("%d ", topK[i]);
//	}
//	fclose(fout);
//}
//
//int main()
//{
//	CreateNDate();
//	PrintTopK("data.txt", 10);
//	return 0;
//}
//
