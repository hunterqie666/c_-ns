#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>


////最大公约数
//int Fun1(int a, int b)
//{
//
//	int n = 0;
//	n = a > b ? b : a;
//	while (n > 1)
//	{
//		if ((a % n == 0) && (b % n == 0))
//			return n;
//		n--;
//	}
//	return  n;
//}
//
////最小公倍数
//int Fun2(int a, int b)
//{
//	int n = 0;
//	n = a > b ? a : b;
//	while (1)
//	{
//		if ((n % a == 0) && (n % b == 0))
//			break;
//		n++;
//	}
//	return n;
//}
//
//int main()
//{
//	int a = 0, b = 0, c = 0;
//	scanf("%d %d", &a, &b);
//
//	c = Fun1(a, b);
//	printf("%d\n", c);
//	c = Fun2(a, b);
//	printf("%d", c);
//
//	return 0;
//}


//void print(int ar[3][3])
//{
//	for (int i = 0; i < 3; ++i)
//	{
//		for (int j = 0; j < 3; ++j)
//		{
//			printf("%d ", ar[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//void reverse(int ar[3][3])
//{
//	int tmp;
//	for (int i = 0; i < 3; ++i)
//	{
//		for (int j = 0; j < i; ++j)
//		{
//			if (i != j) 
//			{
//				tmp = ar[i][j];
//				ar[i][j] = ar[j][i];
//				ar[j][i] = tmp;
//			}
//		}
//	}
//}
//
//int main()
//{
//	int array[3][3] =
//	{
//		{1,2,3},
//		{4,5,6},
//		{7,8,9}
//	};
//
//	printf("转置前：\n");
//	print(array);
//
//	reverse(array);
//
//	printf("转置后：\n");
//	print(array);
//
//	return 0;
//}


void Fun(char string1[], char string2[], char string[])
{
	int i, j;
	for (i = 0; string1[i] != '\0'; i++)
	{
		string[i] = string1[i];
	}
	for (j = 0; string2[j] != '\0'; j++)
	{
		string[i + j] = string2[j];
	}
	string[i + j] = '\0';
}

int main()
{
	char s1[200] = { 0 }, s2[100] = { 0 }, s[100] = { 0 };
	
	scanf("%s", s1);
	scanf("%s", s2);

	Fun(s1, s2, s);

	printf("\nThe new string is %s\n", s);
	return 0;
}
