#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int main()
//{
//    int i = 0, j = 0;
//    int t[16] = { 0 };
//    int n;
//    float sum = 0;
//    scanf("%d", &n);
//    while (n)
//    {
//        if (n % 2 == 0 || n == 0)
//        {
//            t[i] = 0;
//            sum += t[i];
//        }
//        else
//        {
//            t[i] = 1;
//            sum += t[i];
//        }
//        ++i;
//        n = n / 10;
//    }
//
//    if (sum / (i + 1) == t[0])
//        printf("%d", t[0]);
//    else if (t[i] == 0)
//    {
//        for (j = i; j >= 0; j--)
//        {
//            if (t[j] % 2 != 0)
//                while (j >= 0)
//                {
//                    printf("%d", t[j]);
//                    j--;
//                }
//        }
//    }
//    else
//        while (i >= 0)
//        {
//            printf("%d", t[i]);
//            --i;
//        }
//    return 0;
//}

//int main()
//{
//    int n, i, a[1000], count = 0;
//    scanf("%d", &n);
//    int m = n;
//    for (i = 0; i < m; i++) 
//    {
//        if (n == 0) {
//            break;
//        }
//        a[count] = n % 6;
//        n = n / 6;
//        count++;
//    }
//    for (i = count - 1; i >= 0; i--) 
//    {
//        printf("%d", a[i]);
//    }
//    return 0;
//}

//int main()
//{
//	int h = 0;
//	int m = 0;
//	long int k = 0;
//	scanf("%d:%d %ld", &h, &m, &k);
//	
//	if (((h >= 0) && (h <= 23)) && ((m >= 0) && (m <= 59)) && (k >= 1))
//	{
//		int ah = 0;
//		int am = 0;
//		ah = k / 60;
//		am = k % 60;
//		if ((m + am) >= 60)
//		{
//			ah += ((m + am) / 60);
//			am = (m + am) % 60;
//		}
//		else
//		{
//			am += m;
//		}
//		h = (h + ah) % 24;
//		printf("%02d:%02d\n", h, am);
//	}
//	return 0;
//}


char Iswin(char board[3][3], int row, int col)
{
    int i = 0;
    for (i = 0; i < row; i++)
    {
        if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != 'O')
            return board[i][0];
    }

    for (i = 0; i < col; i++)
    {
        if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != 'O')
            return board[0][i];
    }

    if (((board[0][0] == board[1][1] && board[1][1] == board[2][2]) ||
        (board[0][2] == board[1][1] && board[1][1] == board[2][0])) && board[1][1] != 'O')
        return board[1][1];
    return 'Q';
}


int main()
{
    char board[3][3] = { 0 };
    int i = 0;
    for (i = 0; i < 3; i++)
    {
        int j = 0;
        for (j = 0; j < 3; j++)
        {
            scanf(" %c", &board[i][j]);
        }
    }
    char ret = Iswin(board, 3, 3);
    if (ret == 'K')
        printf("KiKi wins!\n");
    else if (ret == 'B')
        printf("BoBo wins!\n");
    else
        printf("No winner!\n");
    return 0;
}