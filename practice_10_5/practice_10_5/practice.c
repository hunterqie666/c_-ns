#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>


//void GetMemory(char* p)
//{
//	p = (char*)malloc(100);
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(str);
//	strcpy(str, "hello world");
//	printf(str);
//}

//char* GetMemory(void)
//{
//	char p[] = "hello world";
//	return p;
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory();
//	printf(str);
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int arr1[2] = { 0 };
//	int z = 0;
//	for (int i = 0; i < sz; i++)
//	{
//		if (arr[i] == 0)
//		{
//			continue;
//		}
//		int j = 0;
//		for ( j = i + 1;j < sz ;j++)
//		{
//			if (arr[j] == 0)
//			{
//				continue;
//			}
//			if (arr[i] == arr[j])
//			{
//				arr[j] = 0;
//				break;
//			}
//		}
//		if ( j == sz )
//		{
//			arr1[z] = arr[i];
//			z++;
//		}
//	}
//	for (int i = 0; i < 2; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}

//#include <assert.h>
//int my_atoi(const char* str)
//{
//	char* a = str;
//	assert(str);
//	int z = 0;
//	while (*str++ != '\0')
//	{
//		if (*str >= 48 && *str <= 57)
//		{
//			z += *str - 48;
//			z *= 10;
//		}
//		else if (*str == " ")
//		{
//			continue;
//		}
//	}
//	if (*a == '-')
//	{
//		return -z / 10;
//	}
//	return z / 10;
//}
//
//int main()
//{
//	char str[] = "-1314520";
//	int num = my_atoi(str);
//	printf("%d", num);
//	return 0;
//}

#include <assert.h>
//char* my_strncpy(char* dest, const char* src, size_t num)
//{
//	assert(dest && src);
//	char* ps = dest;
//	while (num--)
//	{
//		//��ָֹ��Խ��
//		if (*src == '\0')
//		{
//			*dest++ = '\0';
//		}
//		else
//			*dest++ = *src++;
//	}
//	return ps;
//}
//
//int main()
//{
//	char arr1[] = "--------------";
//	char arr2[] = "ctxyyds";
//	my_strncpy(arr1, arr2, 7);
//	printf("%s\n", arr1);
//	return 0;
//}


char* my_strncpy(char* dest, const char* src, size_t num)
{
	assert(dest && src);
	char* ps = dest;
	while (num--)
	{
		if (*src == '\0')
		{
			*dest++ = '\0';
		}
		else
			*dest++ = *src++;
	}
	return ps;
}

int main()
{
	char arr1[] = "------------";
	char arr2[] = "ctxyyds";
	my_strncpy(arr1, arr2, 8);
	printf("%s\n", arr1);
	return 0;
}
