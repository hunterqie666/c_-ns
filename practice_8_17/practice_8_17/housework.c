#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include<stdlib.h>
#include<string.h>


//int main()
//{
//    int n = 0, i = 0;
//    char num[20];
//
//    printf("请输入一个8进制数:");
//    gets(num);
//
//    while (num[i] != '\0') 
//    {
//        n = n * 8 + num[i] - '0';
//        i++;
//    }
//    printf("转化之后：%d\n", n);
//
//    return 0;
//}

//
//char* strconnect(char* str1, char* str2)
//{
//    char* str;
//    str = (char*)malloc(strlen(str1) + strlen(str2) + 1);
//    str[0] = '\0';
//    strcat(str, str1);
//    strcat(str, str2);
//    return str;
//}
//
//int main()
//{
//    char str1[20] = { 0 }, str2[20] = { 0 };
//    char* str = 0;
//    printf("请输入两个字符串");
//    scanf("%s%s", str1, str2);
//    str = strconnect(str1, str2);
//    printf("连接后的字符串为:");
//    puts(str);
//    return 0;
//}


//#define M 5
//int main()
//{
//    int i = 0, j = 0, t = 0;
//    int arr[M] = { 0 };
//    for (i = 0; i < M; i++)
//    {
//      scanf("%d", &arr[i]);
//    }
//
//    i = 0;
//    j = M - 1;
//    while (i < j)
//    {
//        t = arr[i];
//        arr[i] = arr[j];
//        arr[j] = t;
//        i++;
//        j--;
//    }
//
//    for (i = 0; i < M; i++) 
//    {
//        printf("%d\n", arr[i]);
//    }
//}


int main()
{
    int a[100][100] = { 0 };
    int n;
    int row = 0, col = 0; 
    scanf("%d", &n);
    for (int i = 0; i < n; i++) 
    {
        for (int j = 0; j < n; j++) 
        {
            scanf("%d", &a[i][j]);
        }
    }
    int max, min;
    for (int i = 0; i < n; i++)
    {
        max = a[i][0];
        for (int j = 0; j < n; j++)
        {
            if (a[i][j] > max) 
            {
                max = a[i][j];
                col = j;
            }
        }
        min = a[0][col];
        for (int k = 0; k < n; k++) 
        {
            if (a[k][col] < min) 
            {
                min = a[k][col];
                row = k;
            }
        }
        if (min == max) 
        {
            printf("%d %d", row, col);
            break;
        }
    }
    if (min != max)
    {
        printf("NO");
    }
    return 0;
}