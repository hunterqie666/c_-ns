#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>

//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	i = 0;
//	int j = 0;
//	while (j<10)
//	{
//		if (arr[j] % 2 == 1)
//		{
//			int tmp = arr[i];
//			arr[i] = arr[j];
//			arr[j] = tmp;
//			i++;
//		}
//		j++;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//char* my_strcpy(char* dest, const char* src)
//{
//		assert(src);
//      assert(dest);
//		char* ret = dest;
//		while (*dest++ = *src++)
//		{
//			;
//		}
//		return ret;
//}
//int main()
//{
//	char arr1[] = "xxxxxxxxxxxx";
//	char arr2[] = "CTX YYDS";
//	printf("%s", my_strcpy(arr1, arr2));
//	return 0;
//}

size_t my_strlen(const char* arr)
{
	assert(arr);
	size_t count = 0;
	while (*arr++)
	{
		count++;
	}
	return count;
}

int main()
{
	char arr[] = "CTX YYDS";
	size_t ret = my_strlen(arr);
	printf("%zd", ret);
	return 0;
}