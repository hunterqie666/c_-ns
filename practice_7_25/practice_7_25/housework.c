#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>


//void init(int arr[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		arr[i] = 0;
//	}
//}
//void print(int arr[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//void reverse(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//		int tmp = 0;
//		tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	int arr[5] = { 1,2,3,4,5 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("把数组arr初始化0:>");
//	init(arr,sz);
//	
//	print(arr, sz);
//	
//	//逆序
//	int arr1[5] = { 1,2,3,4,5 };
//	printf("逆序前的数组:>");
//	print(arr1, sz);
//
//	printf("逆序后的数组:>");
//	
//	reverse(arr1, sz);
//	print(arr1, sz);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,3,2,5,6,7,8,4,10,9 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("冒泡排序前:");
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n冒泡排序后:");
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		for ( j = 0; j < sz-1-i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = 0;
//				tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

int main()
{
	int A[5] = { 1,2,3,4,5 };
	int B[5] = { 5,4,3,2,1 };
	int arr3[5] = { 0 };
	printf("A数组原来的内容:>");
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", A[i]);
	}
	printf("\nB数组原来的内容:>");
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", B[i]);
	}
	
	for (int i = 0; i < 5; i++)
	{
		arr3[i] = A[i];
		A[i] = B[i];
		B[i] = arr3[i];
	}
	
	printf("\n---------------------------");
	
	printf("\nA数组交换后的内容:>");
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", A[i]);
	}
	printf("\nB数组交换后的内容:>");
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", B[i]);
	}
	return 0;
}