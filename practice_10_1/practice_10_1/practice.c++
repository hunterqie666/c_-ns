#include <stdio.h>
#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdint.h>
#include <easyx.h>
#include <math.h>

//#define PI 3.14
//
//void fivePointedStar(int radius, double startAngle)
//{
//	double delta = 2 * PI / 5;
//
//	POINT points[5];
//	for (int i = 0; i < 5; i++)
//	{
//		points[i].x = cos(startAngle + i * delta * 2) * radius;
//		points[i].y = sin(startAngle + i * delta * 2) * radius;
//	}
//	solidpolygon(points, 5);
//}
//
//int main(void)
//{
//	int width = 900;
//
//	int height = width / 3 * 2;
//
//	int grid = width / 2 / 15;
//	initgraph(width, height);
//	setbkcolor(RED);
//	cleardevice();
//
//	setaspectratio(1, -1);
//	setfillcolor(YELLOW);
//	setpolyfillmode(WINDING);
//
//	setorigin(grid * 5, grid * 5);
//	fivePointedStar(grid * 3, PI / 2);
//
//	setorigin(grid * 10, grid * 2);
//	double startAngle = atan(3.0 / 5.0) + PI;
//	fivePointedStar(grid, startAngle);
//
//	setorigin(grid * 12, grid * 4);
//	startAngle = atan(1.0 / 7.0) + PI;
//	fivePointedStar(grid, startAngle);
//
//	setorigin(grid * 12, grid * 7);
//	startAngle = -atan(2.0 / 7.0) + PI;
//	fivePointedStar(grid, startAngle);
//
//	setorigin(grid * 10, grid * 9);
//	startAngle = -atan(4.0 / 5.0) + PI;
//	fivePointedStar(grid, startAngle);
//
//	getchar();
//	closegraph();
//
//	return 0;
//}


//#include <graphics.h>
//#include <time.h>
//#include <conio.h>
//
//int main()
//{
//	// 设置随机种子
//	srand((unsigned)time(NULL));
//
//	// 初始化图形模式
//	initgraph(640, 480);
//
//	int  x, y;
//	char c;
//
//	settextstyle(16, 8, _T("Courier"));	// 设置字体
//
//	// 设置颜色
//	settextcolor(GREEN);
//	setlinecolor(BLACK);
//
//	for (int i = 0; i <= 479; i++)
//	{
//		// 在随机位置显示三个随机字母
//		for (int j = 0; j < 3; j++)
//		{
//			x = (rand() % 80) * 8;
//			y = (rand() % 20) * 24;
//			c = (rand() % 26) + 65;
//			outtextxy(x, y, c);
//		}
//
//		// 画线擦掉一个像素行
//		line(0, i, 639, i);
//
//		Sleep(10);					// 延时
//		if (i >= 479)	i = -1;
//		if (_kbhit())	break;		// 按任意键退出
//	}
//
//	// 关闭图形模式
//	closegraph();
//	return 0;
//}


// 编译环境：Visual C++ 6.0~2022，EasyX_2023大暑版
// https://easyx.cn
//
#include <graphics.h>
#include <time.h>
#include <conio.h>

#define MAXSTAR 200	// 星星总数

struct STAR
{
	double	x;
	int		y;
	double	step;
	int		color;
};

STAR star[MAXSTAR];

// 初始化星星
void InitStar(int i)
{
	star[i].x = 0;
	star[i].y = rand() % 480;
	star[i].step = (rand() % 5000) / 1000.0 + 1;
	star[i].color = (int)(star[i].step * 255 / 6.0 + 0.5);	// 速度越快，颜色越亮
	star[i].color = RGB(star[i].color, star[i].color, star[i].color);
}

// 移动星星
void MoveStar(int i)
{
	// 擦掉原来的星星
	putpixel((int)star[i].x, star[i].y, 0);

	// 计算新位置
	star[i].x += star[i].step;
	if (star[i].x > 640)	InitStar(i);

	// 画新星星
	putpixel((int)star[i].x, star[i].y, star[i].color);
}

// 主函数
int main()
{
	srand((unsigned)time(NULL));	// 随机种子
	initgraph(640, 480);			// 创建绘图窗口

	// 初始化所有星星
	for (int i = 0; i < MAXSTAR; i++)
	{
		InitStar(i);
		star[i].x = rand() % 640;
	}

	// 绘制星空，按任意键退出
	while (!_kbhit())
	{
		for (int i = 0; i < MAXSTAR; i++)
			MoveStar(i);
		Sleep(20);
	}

	closegraph();					// 关闭绘图窗口
	return 0;
}

//int main()
//{
//    
//    int num1 = 0;
//    int num2 = 0;
//    scanf_s("%d %d", &num1, &num2);
//    int arr[10][10] = { 0 };
//
//    for (int i = 0; i < num1; i++) 
//    {
//        for (int j = 0; j < num2; j++)
//        {
//            scanf_s("%d", &arr[i][j]);
//        }
//    }
//
//    int z = 0;
//    int k = 0;
//    int MAX = arr[0][0];
//
//    for (int i = 0; i < num1; i++)
//    {
//        for (int j = 0; j < num2; j++) 
//        {
//            if (MAX < arr[i][j]) 
//            {
//                MAX = arr[i][j];
//                z = i;
//                k = j;
//            }
//        }
//    }
//    z++;
//    k++;
//    printf("%d %d",z, k);
//    return 0;
//}
