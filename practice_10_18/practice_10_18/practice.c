#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>


//#define N 4
//#define Y(n) ((N+2)*n) 
//
//int main()
//{
//	printf("%d", 2 * (N + Y(5 + 1)));
//	//			 2 * (4 + (4 + 2) * 5 + 1)
//	return 0;
//}


//#define A 2+2
//#define B 3+3
//#define C A*B
//int main()
//{
//	printf("%d\n", C);
//				// 2 + 2 * 3 + 3 
//	return 0;
//}


//#include <stdio.h>     
//#include <stddef.h>    
//
//struct foo {
//	char a;
//	char b[10];
//	char c;
//};
//
//int main()
//{
//	printf("offsetof(struct foo,a) is %d\n", (int)offsetof(struct foo, a));
//	printf("offsetof(struct foo,b) is %d\n", (int)offsetof(struct foo, b));
//	printf("offsetof(struct foo,c) is %d\n", (int)offsetof(struct foo, c));
//
//	return 0;
//}



//#define OFFSETOF(type,mem) (size_t)&(((type*)0)->mem)
//struct S
//{
//	char a;
//	int b[10];
//	char c;
//};
//
//int main()
//{
//	printf("offsetof(struct foo,a) is %d\n", OFFSETOF(struct S, a));
//	printf("offsetof(struct foo,b) is %d\n", OFFSETOF(struct S, b));
//	printf("offsetof(struct foo,c) is %d\n", OFFSETOF(struct S, c));
//	return 0;
//}

					//奇数							偶数
#define SWAP(n)   (n = ((n & 0x55555555)<<1) + ((n & 0xaaaaaaaa)>>1))
int main()
{
	int a = 10;
	printf("第一次：%d\n", SWAP(a));
	printf("第二次：%d", SWAP(a));
	return 0;
}

