#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int main()
//{
//	int n, k;
//	scanf("%d %d", &n, &k);
//	int i, j, count = 0;
//	for (i = 2; i <= n; i++)
//	{
//		for (j = 1; j <= n; j++)
//		{
//			if (i % j >= k)
//			{
//				count++;
//			}
//		}
//	}
//	printf("%d", count);
//	return 0;
//}


//int findPeakElement(int* num, int numslen)
//{
//    // write code here
//    //边界情况处理，1个元素前后都是负无穷 以及 0号位置大于1号位置，-1位置负无穷的情况
//    if (numslen == 1)
//        return 0;
//    //末尾位置数据大于上一个位置数据，而nums[numsLen]负无穷的情况
//    if (num[numslen - 1] > num[numslen - 2])
//        return numslen - 1;
//    int left = 0;
//    int right = numslen - 1;
//    int mid = 0;
//    while (left < right)
//    {
//        mid = left + (right - left) / 2;
//        if (num[mid] > num[mid + 1])//中间比右边大，意味着左肯包包括当前位置定有个峰值
//            right = mid;
//        else//否则在右边肯定有个峰值
//            left = mid + 1;
//    }
//    return left;
//}


//int main()
//{
//	long n, k;
//	while (~scanf("%ld %ld", &n, &k))
//	{
//		if (k == 0)
//		{
//			printf("%ld\n", n * n);//任意数对的取模结果都是大于等于0的
//			continue;
//		}
//		long count = 0;
//		for (long y = k + 1; y <= n; y++)
//		{
//			count += ((n / y) * (y - k)) + ((n % y < k) ? 0 : (n % y - k + 1));
//		}
//		printf("%ld\n", count);
//	}
//	return 0;
//}
