#include "List.h"


//void Test1()
//{
//	ListNode* plist = LTInit();
//	ListPushBack(plist, 1);
//	ListPushBack(plist, 2);
//	ListPushBack(plist, 3);
//	ListPushBack(plist, 5);
//	ListPushBack(plist, 4);
//
//	ListPrint(plist);
//
//	ListPushFront(plist, 110);
//
//	ListPrint(plist);
//
//	ListPopFront(plist);
//
//	ListPrint(plist);
//	//ListPopBack(plist);
//
//	//ListPrint(plist);
//}

void Test2()
{
	ListNode* plist = LTInit();
	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 5);
	ListPushBack(plist, 4);

	ListPrint(plist);

	ListNode* pos = ListFind(plist, 3);
	if (pos)
	{
		pos->data *= 10;
	}
	

	ListInsert(pos, 2003);
	ListPrint(plist);

	ListErase(pos);
	ListPrint(plist);

	ListDestory(plist);
}

int main()
{
	//Test1();
	Test2();
	return 0;
}