#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>


//int Fun(int n)
//{
//	if (n == 5)
//		return 2;
//	else
//		return 2 * Fun(n + 1);
//}
//
//int main()
//{
//	printf("%d", Fun(2));
//	return 0;
//}

//斐波纳契
//int Fib1(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return Fib1(n - 1) + Fib1(n - 2);
//}
// 
//int Fib2(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while (n > 2)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
// 
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	printf("递归方法：%d\n", Fib1(n));
//	printf("非递归方法：%d", Fib2(n));
//	return 0;
//}

//n的k次方
//int Fun(int n, int k)
//{
//	if (k <= 0)
//		return 1;
//	else
//		return n * Fun(n , k - 1);
//}
//
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	printf("%d", Fun(n, k));
//	return 0;
//}

//int DigitSum(int n)
//{
//	if (n <= 9)
//		return n;
//	else
//		return n % 10 + DigitSum(n / 10);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	printf("%d", DigitSum(n));
//	return 0;
//}

//int my_strLen(char str[]) 
//{
//	if (str[0] == '\0') 
//	{
//		return 0;
//	}
//	return 1 + my_strLen(str + 1);
//}
//void reverse_string(char str[]) 
//{
//	int len = my_strLen(str);
//	char tem = *str;
//	*str = *(str + len - 1);
//	*(str + len - 1) = '\0';
//	if (my_strLen(str) > 0) 
//	{
//		reverse_string(str + 1);
//	}
//	*(str + len - 1) = tem;
//}
//int main() {
//	char str[] = "ctxyyds";
//	printf("改变前：%s\n", str);
//	reverse_string(str);
//	printf("改变后：%s\n", str);
//	return 0;
//}

//int my_strlen1(char* str)
//{
//    if (*str != '\0')
//    {
//        return 1 + my_strlen1(str + 1);
//    }
//    else
//    {
//        return 0;
//    }
//}
//int my_strlen2(char* str)
//{
//    int count = 0;
//    while (*str != '\0')
//    {
//        count++;
//        str++;
//    }
//    return count;
//}
//int main()
//{
//	char arr[] = "ctxyyds";
//	printf("递归方法：%d\n", my_strlen1(arr));
//  printf("非递归方法：%d", my_strlen2(arr));
//	return 0;
//}

//int Fun1(int n)
//{
//	if (n <= 1)
//		return 1;
//	else
//		return n * Fun1(n - 1);
//}
//int Fun2(int n)
//{
//	int a = 1;
//	for (int i = 1; i <= n; i++)
//	{
//		a *= i;
//	}
//	return a;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	printf("递归方法：%d\n", Fun1(n));
//	printf("非递归方法：%d", Fun2(n));
//	return 0;
//}

//void print(int a)
//{
//	if (a > 9)
//		 print(a/10);
//	printf("%d ", a % 10);
//}
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	print(a);
//	return 0;
//}