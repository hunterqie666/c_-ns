#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <math.h>


//int Max3(a, b, c)       
//{
//
//    if (a >= b && a >= c)
//    {
//        return a;
//    }
//    else if (b >= c && b >= a)
//    {
//        return b;
//    }
//    else
//    {
//        return c;
//    }
//}
//
//int main()
//{
//    float n = 0;
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    scanf("%d %d %d", &a, &b, &c);
//    n = (float)Max3(a + b, b, c) / (Max3(a, b + c, c) + Max3(a, b, b + c));
//    printf("%.2f\n", n);
//    return 0;
//}

//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	long long b = a * 3.156*pow(10,7);
//	printf("%lld", b);
//	return 0;
//}

//int main()
//{
//	double a, b, c;
//	scanf("%lf %lf %lf", &a, &b, &c);
//	double sum = a + b + c;
//	double n = sum / 3;
//	printf("%.2lf %.2lf", sum, n);
//	return 0;
//}

//int main()
//{
//	double a, b, c, d, e, f, g;
//	while ((scanf("%lf %lf %lf %lf %lf %lf %lf", &a, &b, &c, &d, &e, &f, &g))==7)
//	{
//		double n = 0;
//		int i = 0, j = 0, k = 0;
//		double arr[7] = { a,b,c,d,e,f,g };
//		double max = arr[0];
//		double min = arr[0];
//		for ( i = 1; i < 7; i++)
//		{
//			if (max < arr[i])
//			{
//				max = arr[i];
//				j = i;
//			}
//
//			if (min>arr[i])
//			{
//				min = arr[i];
//				k = i;
//			}
//		}
//		arr[k] = 0;
//		arr[j] = 0;
//		for ( i = 0; i < 7; i++)
//		{
//			n += arr[i];
//		}
//		n /= 5;
//		printf("%.2lf\n", n);
//	}
//	return 0;
//}

int main()
{
	int n = 0;
	double arr[100] = { 0 };
	
	scanf("%d", &n);
	for (int i = 0; i < n; i++)
	{
		scanf("%lf", &arr[i]);
	}

	double max = arr[0], min = arr[0], average = arr[0];
	for (int i = 1; i < n; i++)
	{
		if (max < arr[i])
		{
			max = arr[i];
		}
		if (min > arr[i])
		{
			min = arr[i];
		}
		average += arr[i];
	}

	printf("%.2lf %.2lf %.2lf", max, min, average / n);
	return 0;
}