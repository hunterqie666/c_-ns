#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>

//* @param nums int整型一维数组
//* @param numsLen int nums数组长度
//* @return int整型
//* /
//int minNumberInRotateArray(int* nums, int numsLen) {
//	int min = *nums;
//	for (int i = 1; i < numsLen; i++)
//	{
//		if (min > *(nums + i))
//		{
//			min = *(nums + i);
//		}
//	}
//	return min;
//}

//输入 65 14
//int main()
//{
//	int m, n;
//	printf("Enter m,n;");
//	scanf("%d%d", &m, &n);
//	while (m != n) 
//	{
//		while (m > n) 
//			m = m - n; //51 37 23 9 4 3 2 1
//		while (n > m)
//			n = n - m; // 5 1 
//	}
//	printf("m=%d\n", m);
//	return 0;
//}


////键盘输入 ADescriptor<回车>
//int main()
//{
//	char c;
//	int v0 = 0, v1 = 0, v2 = 0;	
//	do
//		{
//			switch (c = getchar())
//			{
//			case'a':case'A':
//			case'e':case'E':
//			case'i':case'I':
//			case'o':case'O':
//			case'u':case'U':v1 += 1;
//			default:v0 += 1; v2 += 1;
//			}
//		} while (c != '\n');
//		printf("v0=%d,v1=%d,v2=%d\n", v0, v1, v2);//12 4 12
//		return 0;
//}


//int main()
//{
//	char ch[80] = "123abcdEFG*&";
//	int j;
//	puts(ch);
//	for (j = 0; ch[j] != '\0'; j++)
//		if (ch[j] >= 'A' && ch[j] <= 'Z')
//			ch[j] = ch[j] + 'e' - 'E';
//	puts(ch);
//	return 0;
//}

//int main()
//{
//	int t = 0;
//	while (printf("*"))
//	{
//		t++;
//		if (t < 3)
//			break;
//	}
//	return 0;
//}

////输入 1abcedf2df
//int main()
//{
//	char ch;
//	while ((ch = getchar()) != '\n')
//	{
//		if (ch % 2 != 0 && (ch >= 'a' && ch <= 'z'))
//			ch = ch - 'a' + 'A';
//		putchar(ch);
//	}
//	printf("\n");
//	return 0;
//}

//* @param nums int整型一维数组
//* @param numsLen int nums数组长度
//* @param k int整型
//* @return int整型
//* /
//int GetNumberOfK(int* nums, int numsLen, int k) {
//    int count = 0;
//    for (int i = 0; i < numsLen; i++)
//    {
//        if (*(nums + i) == k)
//            count++;
//    }
//    return count;
//}
