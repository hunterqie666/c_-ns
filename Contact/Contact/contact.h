#pragma once

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#define NAME_MAX 20
#define SEX_MAX 5
#define TELE_MAX 12
#define ADDR_MAX 30

#define MAX 100


#define DEFAULT_SZ 3
#define DEFAULT_INC 2
typedef struct PeoInfo
{
	char name[NAME_MAX];
	int age;
	char sex[SEX_MAX];
	char tele[TELE_MAX];
	char addr[ADDR_MAX];

}PeoInfo;


//静态
//typedef struct Contact
//{
//	PeoInfo data[MAX];
//	int sz;
//}Contact;

//动态
typedef struct Contact
{
	PeoInfo* data;
	int sz;
	int capacity;
}Contact;

//初始化
void InitC(Contact* pc);

//增加
void AddC(Contact* pc);

//显示
void ShowC(const Contact* pc);

//删除
void DelC(Contact* pc);

//查找
void SeaC(Contact* pc);

//修改
void ModC(Contact* pc);

//排序
void SortC(Contact* pc);

//销毁
void DestoryContact(Contact* pc);

//保存信息到文件
void SaveC(Contact* pc);

//加载文件信息到通讯录
void LoadC(Contact* pc);