#define  _CRT_SECURE_NO_WARNINGS 1
#include "contact.h"

void menu()
{
	printf("********************************\n");
	printf("**** 1. add        2. del   ****\n");
	printf("**** 3. search     4.modify ****\n");
	printf("**** 5. show       6. sort  ****\n");
	printf("**** 0. exit                ****\n");
	printf("********************************\n");
}

enum s
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SHOW,
	SORT
};

int main()
{
	int input = 0;

	Contact con;

	InitC(&con);


	do
	{
		menu();
		printf("请输入你的选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case ADD://增加
			AddC(&con);
			break;

		case DEL://删除
			DelC(&con);
			break;

		case SEARCH://查询
			SeaC(&con);
			break;

		case MODIFY://修改
			ModC(&con);
			break;

		case SHOW://展示
			ShowC(&con);
			break;

		case SORT://排序
			SortC(&con);
			break;

		case EXIT://退出
			SaveC(&con);
			DestoryContact(&con);
			printf("退出通讯录\n");
			break;

		default:
			printf("选择错误，重新选择\n");
			break;
		}
	} while (input);
	return 0;
}




